/* 
 * File:   system.h
 * Author: Martins
 *
 * System header file
 *
 * Created on otrdiena, 2014, 9 septembris, 19:21
 */

#ifndef SYSTEM_H
#define	SYSTEM_H

#ifdef	__cplusplus
extern "C" {
#endif

#ifndef FCY
    #define FCY  10000000   // FCY is 20/2 =  10 MHz
#endif


// Clock frequency values
// These directly influence timed events using the Tick module.  They also are used for UART and SPI baud rate generation.
// p.165
#define GetSystemClock()		(20000000ul)			// Hz
#define GetInstructionClock()	(GetSystemClock()/2)	// Normally GetSystemClock()/4 for PIC18, GetSystemClock()/2 for PIC24/dsPIC, and GetSystemClock()/1 for PIC32.  Might need changing if using Doze modes.
#define GetPeripheralClock()	(GetSystemClock()/2)	// Normally GetSystemClock()/4 for PIC18, GetSystemClock()/2 for PIC24/dsPIC, and GetSystemClock()/1 for PIC32.  Divisor may be different if using a PIC32 since it's configurable.


#ifdef	__cplusplus
}
#endif

#endif	/* SYSTEM_H */

