/* 
 * File:   main.c
 * Author: Martins
 *
 * Created on svētdiena, 2014, 17 augusts, 15:53
 */

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <libpic30.h>

#include <tcpip.h>
#include <pps.h>
#include "main.h"
#include "wsn_server.h"


/** Config */
_FBS(BWRP_WRPROTECT_OFF); // Boot Segment may be written
_FGS(GWRP_OFF & GSS_OFF);
_FOSCSEL(FNOSC_PRI & IESO_OFF); // FNOSC specify the POSC clock source at a POR.
_FOSC(POSCMD_HS & IOL1WAY_ON & FCKSM_CSDCMD); // POSCMD specify the Primary Oscillator mode. Clock Switching and Fail-Safe Clock Monitor are disabled
_FWDT(FWDTEN_OFF & WDTPOST_PS32768 & WINDIS_OFF & WDTPRE_PR128); // Watchdog. Off. WDT Timeout 131.072 seconds. No window mode.
_FPOR(FPWRT_PWR128);
_FICD(ICS_PGD2 & JTAGEN_OFF); // Programming on PGEC2/PGED2.

// Declare AppConfig structure and some other supporting stack variables
APP_CONFIG AppConfig;

unsigned char rxData;
unsigned char test = 0;

/*
 * 
 */
int main(int argc, char** argv) {


    // Hardware Init
    InitBoard();
    InitUART();
    InitIRQ();

    LED_TRIS = 0; // LED output

    TickInit(); // p.161. Init tick timer that manages stack timing.

    // Initialize Stack and application related NV variables into AppConfig.
    InitAppConfig();

    /**
     * This will automatically call the initialization functions for other firmware protocols
     * if they have been enabled in TCPIPConfig.h (i.e. TCPInit() for the TCP protocol, HTTPInit() for HTTP2,...)
     */
    StackInit();


       LED = 0;
    // p.159.
    while(1){
        // each subtask is implemented as state-machine controlled cooperative multitasking function.
        // This task performs normal stack task including checking for incoming packet, type of packet and calling appropriate stack entity to process it.
        
        StackTask(); // each call of StackTask can retrieve one packet from the packet buffer
        StackApplications(); // This will call loaded application modules.
        WsnServer();
    }
    
    return (EXIT_SUCCESS);
}



/********************************************************************
 * APP_CONFIG structure
 * PreCondition:    MPFSInit() is already called.
 * Output:          Write/Read non-volatile config variables.
 *
 * Addresses p.171, flags,  NBNS/SSID
 *******************************************************************/
void InitAppConfig(void){

    // Start out zeroing all AppConfig bytes to ensure all fields are
    // deterministic for checksum generation
    memset((void*)&AppConfig, 0x00, sizeof(AppConfig));

    AppConfig.Flags.bIsDHCPEnabled = 1;
    AppConfig.Flags.bInConfigMode = 1;

    AppConfig.MyMACAddr.v[0] = MY_DEFAULT_MAC_BYTE1;
    AppConfig.MyMACAddr.v[1] = MY_DEFAULT_MAC_BYTE2;
    AppConfig.MyMACAddr.v[2] = MY_DEFAULT_MAC_BYTE3;
    AppConfig.MyMACAddr.v[3] = MY_DEFAULT_MAC_BYTE4;
    AppConfig.MyMACAddr.v[4] = MY_DEFAULT_MAC_BYTE5;
    AppConfig.MyMACAddr.v[5] = MY_DEFAULT_MAC_BYTE6;
    //memcpypgm2ram((void*)&AppConfig.MyMACAddr, (ROM void*)SerializedMACAddress, sizeof(AppConfig.MyMACAddr));

    AppConfig.MyIPAddr.Val = MY_DEFAULT_IP_ADDR_BYTE1 | MY_DEFAULT_IP_ADDR_BYTE2<<8ul | MY_DEFAULT_IP_ADDR_BYTE3<<16ul | MY_DEFAULT_IP_ADDR_BYTE4<<24ul;
    AppConfig.DefaultIPAddr.Val = AppConfig.MyIPAddr.Val;
    AppConfig.MyMask.Val = MY_DEFAULT_MASK_BYTE1 | MY_DEFAULT_MASK_BYTE2<<8ul | MY_DEFAULT_MASK_BYTE3<<16ul | MY_DEFAULT_MASK_BYTE4<<24ul;
    AppConfig.DefaultMask.Val = AppConfig.MyMask.Val;
    AppConfig.MyGateway.Val = MY_DEFAULT_GATE_BYTE1 | MY_DEFAULT_GATE_BYTE2<<8ul | MY_DEFAULT_GATE_BYTE3<<16ul | MY_DEFAULT_GATE_BYTE4<<24ul;
    AppConfig.PrimaryDNSServer.Val = MY_DEFAULT_PRIMARY_DNS_BYTE1 | MY_DEFAULT_PRIMARY_DNS_BYTE2<<8ul  | MY_DEFAULT_PRIMARY_DNS_BYTE3<<16ul  | MY_DEFAULT_PRIMARY_DNS_BYTE4<<24ul;
    AppConfig.SecondaryDNSServer.Val = MY_DEFAULT_SECONDARY_DNS_BYTE1 | MY_DEFAULT_SECONDARY_DNS_BYTE2<<8ul  | MY_DEFAULT_SECONDARY_DNS_BYTE3<<16ul  | MY_DEFAULT_SECONDARY_DNS_BYTE4<<24ul;


}

/**
 * 
 */
void InitBoard(void)
{
    // Configure ANx as Digital pins
    AD1PCFGL = 0x1FFF; // p.136. The AD1PCFGL register has a default value of 0x0000, therefore, all pins that share ANx functions are analog (not digital) by default.

    AD1CON1bits.ADON = 0; // ADC off

    // SPI Config
    SPI1STATbits.SPIEN = 1; // Enable module
    SPI1CON1bits.MSTEN = 1; // Master mode
    SPI1CON1bits.CKE = 1;
    SPI1CON1bits.CKP = 0; // Idle state for clock is a low level
    SPI1CON1bits.DISSCK = 0; // Enable SPI clock

    
    // PPS Remap.
    PPSUnLock;  // Unlock OSCCON.IOLOCK
    // SPI Inputs.
    TRISBbits.TRISB9 = 1; // SDI as Input
    RPINR20bits.SDI1R = 0x9; // SDI1 tied to RP9
    // SPI Outputs
    TRISBbits.TRISB7 = 0; // SCK as Output, because this is Master.
    TRISBbits.TRISB8 = 0; // SDO as Output
    RPOR3bits.RP7R = 0b1000; // RP7 tied to SPI1 Clock Output
    RPOR3bits.RP6R = 0b1001; // RP6 tied to SPI1 Slave Select Output
    RPOR4bits.RP8R = 0b0111; // RP8 tied to SPI1 Data Output

     // External INT1. Push button.
    TRISBbits.TRISB3 = 1;   // This is mandatory if this is input PPS. Because PPS doesnt have priority over the TRISx setting.
    RPINR0bits.INT1R = 0x3; // RP3 (RB3) tied to INT1. Used for button
    // RX PIN. Tied to RB2
    TRISBbits.TRISB2 = 1;
    RPINR18bits.U1RXR = 0x2; // UART1 RX tied to RP2

    // USART TX is wired to RP15. Output PPS mapping.
    RPOR7bits.RP15R = 0b00011; // RP15 tied to UART1 Transmit


    PPSLock;



    //@TODO test without NOSC
    OSCCONbits.NOSC = 0b010; // New oscillator selection bits
    OSCCONbits.CLKLOCK = 1;  // Disable clock switching

    // Doze mode disabled
    CLKDIVbits.DOZEN = 0;
    
    #if defined(SPIRAM_CS_TRIS)
        SPIRAMInit();
    #endif
    #if defined(EEPROM_CS_TRIS)
        XEEInit();
    #endif
    #if defined(SPIFLASH_CS_TRIS)
        SPIFlashInit();
    #endif
}

/**
 * External INT1 IRQ
 */
void __attribute__((interrupt, __shadow__)) _INT1Interrupt(void){

   // LED ^= 1; // Toggle LED
    IFS1bits.INT1IF = 0; // Clear Ext INT1 flag

    // Test uart. SEND data
//    U1TXREG = 0x41;
}

/**
 * UART1 RX IRQ
 */
void __attribute__((interrupt, __shadow__)) _U1RXInterrupt(void){

    // Check if data available in RX buffer
    if (U1STAbits.URXDA == 1)
    {
        rxData = U1RXREG;
        LED ^= 1;
        U1TXREG = rxData;
    }

    IFS0bits.U1RXIF = 0; // Clear flag
}

// IRQs setup
void InitIRQ(void){
    INTCON1bits.NSTDIS = 1; // Disable interrupt nesting
    INTCON2bits.ALTIVT = 0; // Use standard vector table

    // IRQ for button
    INTCON2bits.INT1EP = 1; // IRQ on negative edge
    IFS1bits.INT1IF = 0; // Clear INT1 IRQ flag
    IEC1bits.INT1IE = 1; // Enable INT1 IRQ
}

/**
 * UART init
 */
void InitUART(void){

    // Baud rate generator. We use BAUD Rate 10417. Common for TX and RX. Error is very low.
    // We use this value because 4MHz PIC16 sensor node cannot generate 9600 baud rate without significant error. But it can generate 10471 Baud rate
    U1BRG = 59;

    U1MODEbits.BRGH = 0; // Low speed
    U1MODEbits.PDSEL = 0b00; // 8-bit data, no parity
    U1MODEbits.STSEL = 0; // One stop bit

    // TX Interrupts
    IEC0bits.U1TXIE = 0; // UART1 Transmitter Interrupt disabled

    U1STAbits.UTXISEL0 = 0;
    U1STAbits.UTXISEL1 = 0;
    U1STAbits.UTXINV = 0; // U1TX idle is HIGH

    // Setup RX
    IEC0bits.U1RXIE = 1; // Enable IRQ
    U1STAbits.URXISEL = 0b00; // Receive IRQ flag is set when a single character is received
    U1MODEbits.URXINV = 1; // U1RX idle is LOW

    U1MODEbits.UARTEN = 1; // UART1 is enabled
    U1STAbits.UTXEN = 1; // UART1 TX enabled. Pin controlled by UART module. This also sets U1TXIF. No need to set TRIS setting.


}

