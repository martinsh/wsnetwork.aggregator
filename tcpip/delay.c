/*******************************************************************************
  Company:
    Microchip Technology Inc.

  File Name:
    delay.c

  Summary:
    

  Description:
    General Delay rouines

 *******************************************************************************/


#define __DELAY_C_

#include "tcpip/tcpip.h"

void DelayMs(uint16_t ms)
{
    unsigned char i;
    while(ms--)
    {
        i=4;
        while(i--)
        {
            Delay10us(25);
        }
    }
}

#if defined(__XC16) || defined(__XC32)

void Delay10us(uint32_t dwCount)
{
    volatile uint32_t _dcnt;

    _dcnt = dwCount*((uint32_t)(0.00001/(1.0/GetInstructionClock())/10));
    while(_dcnt--)
    {
        #if defined(__XC32)
            Nop();
            Nop();
            Nop();
        #endif
    }
}

#endif
