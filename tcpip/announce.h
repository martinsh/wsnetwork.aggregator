/*******************************************************************************
  Company:
    Microchip Technology Inc.

  File Name:
    announce.h

  Summary:
    

  Description:
    Announce Module Header
 
 *******************************************************************************/

#ifndef __ANNONCE_H_
#define __ANNONCE_H_

typedef enum {
        DISCOVERY_HOME = 0,
        DISCOVERY_LISTEN,
        DISCOVERY_REQUEST_RECEIVED,
        DISCOVERY_DISABLED
} DiscoverySM;

void    AnnounceInit(void);
void    AnnounceIP(void);
void    DiscoveryTask(void);

#endif
