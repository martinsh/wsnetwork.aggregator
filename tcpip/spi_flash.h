/*******************************************************************************
  Company:
    Microchip Technology Inc.

  File Name:
    spi_flash.h

  Summary:
     - Tested to be compatible with SST25VF016B
     - Expected compatibility with other SST (Microchip) SST25 series
       devices

  Description:
     SPI Flash Memory Driver Header

 *******************************************************************************/


#ifndef __SPIFLASH_H_
#define __SPIFLASH_H_

#define SPI_FLASH_SECTOR_SIZE       (4096ul)
#define SPI_FLASH_PAGE_SIZE         (0ul)       // SST has no page boundary requirements

#define SPI_FLASH_SECTOR_MASK       (SPI_FLASH_SECTOR_SIZE - 1)

#if defined(SPIFLASH_CS_TRIS)
    void SPIFlashInit(void);
    void SPIFlashReadArray(uint32_t dwAddress, uint8_t *vData, uint16_t wLen);
    void SPIFlashBeginWrite(uint32_t dwAddr);
    void SPIFlashWrite(uint8_t vData);
    void SPIFlashWriteArray(uint8_t *vData, uint16_t wLen);
    void SPIFlashEraseSector(uint32_t dwAddr);
#endif

    // If the above funtions are called without SPIFLASH_CS_TRIS's definition, it
    // means that you either have an error in your system_config.h or
    // tcpip_config.h definitions. The code is attempting to call a function
    // that can't possibly work because you have not specified what pins and SPI
    // module the physical SPI EEPROM chip is connected to. Alternatively, if
    // you don't have an SPI EERPOM chip, it means you have enabled a stack
    // feature that requires SPI EEPROM hardware. In this case, you need to edit
    // tcpip_config.h and disable this stack feature. The linker error tells you
    // which object file this error was generated from. It should be a clue as
    // to what feature you need to disable.

#endif
