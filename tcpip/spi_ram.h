/*******************************************************************************
  Company:
    Microchip Technology Inc.

  File Name:
    spi_ram.h

  Summary:
    -Tested with AMI Semiconductor N256S0830HDA

  Description:
    Data SPI RAM Access Routines

 *******************************************************************************/


#ifndef __SPIRAM_H_
#define __SPIRAM_H_

#if defined(SPIRAM_CS_TRIS)
    void SPIRAMInit(void);
    void SPIRAMGetArray(uint16_t wAddress, uint8_t *vData, uint16_t wLength);
    void SPIRAMPutArray(uint16_t wAddress, uint8_t *vData, uint16_t wLength);

    #define SPIRAMPutString(a,b)            SPIRAMPutArray(a, strlen((char*)b))

    #if defined(__XC8)
        void SPIRAMPutROMArray(uint16_t wAddress, ROM uint8_t *vData, uint16_t wLength);
        #define SPIRAMPutROMString(a,b)     SPIRAMPutROMArray(a, strlenpgm((ROM char*)b))
    #else
        #define SPIRAMPutROMString(a,b)     SPIRAMPutArray(a, strlen((char*)b))
        #define SPIRAMPutROMArray(a,b,c)    SPIRAMPutROMArray(a, b, c)
    #endif
#endif

    // If the above funtions are called without SPIRAM_CS_TRIS's definition, it
    // means that you either have an error in your system_config.h or
    // tcpip_config.h definitions. The code is attempting to call a function
    // that can't possibly work because you have not specified what pins and SPI
    // module the physical SPI EEPROM chip is connected to. Alternatively, if
    // you don't have an SPI EERPOM chip, it means you have enabled a stack
    // feature that requires SPI EEPROM hardware. In this case, you need to edit
    // tcpip_config.h and disable this stack feature. The linker error tells you
    // which object file this error was generated from. It should be a clue as
    // to what feature you need to disable.

#endif
