/*******************************************************************************
  Company:
    Microchip Technology Inc.

  File Name:
    delay.h

  Summary:
    

  Description:
    General Delay rouines

 *******************************************************************************/


#ifndef __DELAY_H_
#define __DELAY_H_

#if !defined(GetInstructionClock)
    #error GetInstructionClock() must be defined.
#endif

#if defined(__XC16) || defined(__XC32)
    void Delay10us(uint32_t dwCount);
    void DelayMs(uint16_t ms);
#else
    #define Delay10us(x)            \
    do                              \
    {                               \
        unsigned long _dcnt;        \
        _dcnt=x*((unsigned long)(0.00001/(1.0/GetInstructionClock())/6));   \
        while(_dcnt--);             \
    } while(0)
    void DelayMs(uint16_t ms);
#endif

#endif
