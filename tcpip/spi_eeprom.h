/*******************************************************************************
  Company:
    Microchip Technology Inc.

  File Name:
    spi_eeprom.h

  Summary:
    

  Description:
    External Serial Data EEPROM Access Defs.

 *******************************************************************************/


#ifndef __SPI_EEPROM_H_
#define __SPI_EEPROM_H_

typedef bool XEE_RESULT;
#define XEE_SUCCESS false

#if defined(EEPROM_CS_TRIS)
    void XEEInit(void);
    XEE_RESULT XEEBeginWrite(uint32_t address);
    XEE_RESULT XEEWrite(uint8_t val);
    void XEEWriteArray(uint8_t *val, uint16_t wLen);
    XEE_RESULT XEEEndWrite(void);
    XEE_RESULT XEEBeginRead(uint32_t address);
    uint8_t XEERead(void);
    XEE_RESULT XEEReadArray(uint32_t address, uint8_t *buffer, uint16_t length);
    XEE_RESULT XEEEndRead(void);
    bool XEEIsBusy(void);
#endif

    // If the above funtions are called without EEPROM_CS_TRIS's definition, it
    // means that you either have an error in your system_config.h or
    // tcpip_config.h definitions. The code is attempting to call a function
    // that can't possibly work because you have not specified what pins and SPI
    // module the physical SPI EEPROM chip is connected to. Alternatively, if
    // you don't have an SPI EERPOM chip, it means you have enabled a stack
    // feature that requires SPI EEPROM hardware. In this case, you need to edit
    // tcpip_config.h and disable this stack feature. The linker error tells you
    // which object file this error was generated from. It should be a clue as
    // to what feature you need to disable.

#endif
