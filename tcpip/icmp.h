/*******************************************************************************
  Company:
    Microchip Technology Inc.

  File Name:
    icmp.h

  Summary:
    

  Description:
    ICMP Module Defs for Microchip TCP/IP Stack

 *******************************************************************************/


#ifndef __ICMP_H_
#define __ICMP_H_

void ICMPProcess(NODE_INFO *remote, uint16_t len);

bool ICMPBeginUsage(void);
void ICMPSendPing(uint32_t dwRemoteIP);
void ICMPSendPingToHost(uint8_t * szRemoteHost);
int32_t ICMPGetReply(void);
void ICMPEndUsage(void);

#if defined(__XC8)
    void ICMPSendPingToHostROM(ROM uint8_t * szRemoteHost);
#else
    #define ICMPSendPingToHostROM(a)    ICMPSendPingToHost((uint8_t*)(a))
#endif

#endif
