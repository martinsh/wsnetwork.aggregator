/* 
 * File:   wsn_server.h
 * Author: Martins
 *
 * Created on ceturtdiena, 2014, 18 septembris, 20:32
 */

#ifndef WSN_SERVER_H
#define	WSN_SERVER_H

#ifdef	__cplusplus
extern "C" {
#endif

    void WsnServer(void);


#ifdef	__cplusplus
}
#endif

#endif	/* WSN_SERVER_H */

