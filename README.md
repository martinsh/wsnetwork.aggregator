# Wireless Sensor Network.
## Aggregator node.

Uses TCP/IP Stack from [Microchip Libraries for Applications](http://www.microchip.com/pagehandler/en-us/devtools/mla/home.html) v2014-07-22.
Processor: PIC24HJ128GP502 16-bit MCU
Compiler: XC16