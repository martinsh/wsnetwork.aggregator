/* 
 * File:   main.h
 * Author: Martins
 *
 * Created on svētdiena, 2014, 17 augusts, 15:55
 */

#ifndef MAIN_H
#define	MAIN_H


#ifdef	__cplusplus
extern "C" {
#endif

    void InitAppConfig(void);
    void InitBoard(void);
    void InitIRQ(void);
    void InitUART(void);

    #define LED_TRIS TRISBbits.TRISB5
    #define LED PORTBbits.RB5


#ifdef	__cplusplus
}
#endif

#endif	/* MAIN_H */

