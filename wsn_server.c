/*
 * File:    wsn_server.c
 * Author:  Martins
 * Summary: Wireless Sensor Network Server
 * Created on ceturtdiena, 2014, 18 septembris, 20:35
 */

#include <tcpip.h>

#define SERVER_PORT             (9761u)


enum STATES
{
    SM_DISCONNECTED = 0,
    SM_LISTENING,
    SM_CLOSING
};

enum STATES socketState;
TCP_SOCKET socket;

void WsnServer(void)
{
        // Holds sockets.
    static TCP_SOCKET tcpSockets[MAX_TCP_SOCKETS];
    static unsigned char socketStates[MAX_TCP_SOCKETS];
    static BOOL socketsInitialized = FALSE;

    unsigned char   tcpSocketNr;


    if (!socketsInitialized)
    {
        for (tcpSocketNr = 0; tcpSocketNr < MAX_TCP_SOCKETS; ++tcpSocketNr)
        {
            tcpSockets[tcpSocketNr] = INVALID_SOCKET;
            socketStates[tcpSocketNr] = SM_DISCONNECTED;
        }
        socketsInitialized = TRUE;
    }

    for (tcpSocketNr = 0; tcpSocketNr < MAX_TCP_SOCKETS; ++tcpSocketNr)
    {
        socket = tcpSockets[tcpSocketNr];
        socketState = socketStates[tcpSocketNr];

        // Reset our state if the remote client disconnected from us
        if (socket != INVALID_SOCKET && TCPWasReset(socket))
        {
            socketState = SM_DISCONNECTED;
        }

        switch (socketState)
        {
            case SM_DISCONNECTED:

                /**
                 * 1.9.1.19 TCP
                 * Open listening socket
                 */
                 //@TODO open socket
                socket = TCPOpen(0, TCP_OPEN_SERVER, SERVER_PORT, TCP_PURPOSE_GENERIC_TCP_SERVER);

                // Abort operation if no TCP socket of type TCP_PURPOSE_GENERIC_TCP_SERVER is available
		// If this ever happens, you need to go add one to tcpip_config.h
		if(socket == INVALID_SOCKET)
                    break;

                ++socketState;
                break;

            case SM_LISTENING:

                // See if anyone is connected to us
                if (!TCPIsConnected(socket))
                {
                    break;
                }

                break;

            case SM_CLOSING:
                break;
        }

    }

}