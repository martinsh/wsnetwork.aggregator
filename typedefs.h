/* 
 * File:   typedefs.h
 * Author: Martins
 *
 * Created on otrdiena, 2014, 9 septembris, 22:38
 */

#ifndef TYPEDEFS_H
#define	TYPEDEFS_H

#ifdef	__cplusplus
extern "C" {
#endif

/* UINT is processor specific in length may vary in size */
typedef unsigned int        UINT;
typedef unsigned char       UINT8;
typedef unsigned short int  UINT16;
typedef unsigned long int   UINT32;     /* other name for 32-bit integer. DWORD. */

typedef enum _BOOL { FALSE = 0, TRUE } BOOL;    /* Undefined size */
typedef enum _BIT { CLEAR = 0, SET } BIT;

#ifdef	__cplusplus
}
#endif

#endif	/* TYPEDEFS_H */

