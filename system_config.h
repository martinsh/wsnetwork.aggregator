/*******************************************************************************
  System Specific Definitions

  Company:
    Microchip Technology Inc.

  File Name:
    system_config.h

  Summary:
    System level definitions for the Wireless Sensor Network Aggregation node.

*******************************************************************************/

#ifndef __SYSTEM_CONFIG_H_
#define __SYSTEM_CONFIG_H_

#include <stdio.h>
#include <string.h>

#include "system.h"

#include "tcpip_config.h"

// Hardware I/O pin mappings
//------------------------------------------------------------------------------

// ENC28J60 I/O pins
#define ENC_CS_TRIS		(TRISBbits.TRISB6)	// Comment this line out if you are using the ENC424J600/624J600, MRF24WB0M, or other network controller.
#define ENC_CS_IO		(LATBbits.LATB6)
//#define ENC_RST_TRIS		(TRISDbits.TRISD15)	// Not connected by default.  It is okay to leave this pin completely unconnected, in which case this macro should simply be left undefined.
//#define ENC_RST_IO			(PORTDbits.RD15)
// SPI SCK, SDI, SDO pins are automatically controlled by the
// PIC24/dsPIC/PIC32 SPI module
#define ENC_SCK_TRIS		(TRISBbits.TRISB7)
#define ENC_SDI_TRIS		(TRISBbits.TRISB9)
#define ENC_SDO_TRIS		(TRISBbits.TRISB8)

#define ENC_SPI_IF		(IFS0bits.SPI1IF) // SPI1 Event Interrupt Flag Status bit

#define ENC_SSPBUF		(SPI1BUF)
#define ENC_SPISTAT		(SPI1STAT)
#define ENC_SPISTATbits		(SPI1STATbits)
#define ENC_SPICON1		(SPI1CON1)
#define ENC_SPICON1bits		(SPI1CON1bits)
#define ENC_SPICON2		(SPI1CON2)



//------------------------------------------------------------------------------
// End of /* Hardware I/O pin mappings */


// Compiler information
//------------------------------------------------------------------------------

// Include proper device header file
// PIC24F processor
#include <p24HJ128GP502.h>


// Base RAM and ROM pointer types for given architecture
#define PTR_BASE        unsigned short
#define ROM_PTR_BASE    unsigned short

// Definitions that apply to all except Microchip MPLAB C Compiler for PIC18 MCUs (C18)
#define memcmppgm2ram(a,b,c)    memcmp(a,b,c)
#define strcmppgm2ram(a,b)      strcmp(a,b)
#define memcpypgm2ram(a,b,c)    memcpy(a,b,c)
#define strcpypgm2ram(a,b)      strcpy(a,b)
#define strncpypgm2ram(a,b,c)   strncpy(a,b,c)
#define strstrrampgm(a,b)       strstr(a,b)
#define strlenpgm(a)            strlen(a)
#define strchrpgm(a,b)          strchr(a,b)
#define strcatpgm2ram(a,b)      strcat(a,b)

// Definitions that apply to all 16-bit and 32-bit products
// (PIC24F, PIC24H, dsPIC30F, dsPIC33F, and PIC32)
#define ROM        const

// 16-bit specific defines (PIC24F, PIC24H, dsPIC30F, dsPIC33F)
#define Reset()    asm("reset")
#define FAR        __attribute__((far))



//------------------------------------------------------------------------------
// End of /* Compiler information */

#endif /* __SYSTEM_CONFIG_H_ */
